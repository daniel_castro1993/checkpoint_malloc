#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h>
#include <malloc.h>

int main(int argc, char **argv)
{
  size_t size = 12;
  void *mem = malloc(size);
  printf("Successfully malloc'd %zu bytes at addr %p\n", size, mem);
  assert(mem != NULL);
  free(mem);
  printf("Successfully free'd %zu bytes from addr %p\n", size, mem);

  size = 1024;
  mem = malloc(size);
  printf("Successfully malloc'd %zu bytes at addr %p\n", size, mem);
  assert(mem != NULL);
  free(mem);
  printf("Successfully free'd %zu bytes from addr %p\n", size, mem);

  for (size = 0; size < 1048576; size+=64) {
    mem = malloc(size);
    assert(mem != NULL);
    free(mem);
    if (size % 5120 == 0) {
        printf(".");
        fflush(stdout);
    }
  }
  printf("Successful\n");

  malloc_stats();

  return 0;
}
