FROM debian:buster

WORKDIR /home
RUN apt update && apt install gcc g++ make gdb -y
RUN echo "none	/dev/shm	tmpfs	defaults,size=2	0	0" >> /etc/fstab
# RUN mount -o remount /dev/shm

