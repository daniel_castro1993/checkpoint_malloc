/*
 * Implements malloc library in C.
 * This malloc implemtation has per thread bins.
 * bins are according to four sizes:
 * 8 bytes, 64 bytes, 512 bytes and greated than 512 bytes.
 *
 * memory for size > 512 bytes is allocated through mmap system call.
 * memory blocks are initialized lazily and are appended on free list after
 * free() call.
 *
 * Author: Savan Patel
 * Email : patel.sav@husky.neu.edu
 *
 */

#include <limits.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <mcheck.h>

#ifndef _MALLOC_H
#define _MALLOC_H 1


/*
 * Allocates the memory.
 */
void * malloc(size_t);


/*
 * Free up the memory allocated at pointer p. It appends the block into free
 * list.
 * params: address to pointer to be freed.
 * returns: NONE.
 */
void free(void *p);


/*
 * Allocate size of size bytes for nmemb. Initialize with null bytes.
 * params: total number of elements of size 'size' to be allocated. and size
 *         to allocate.
 * returns: pointer to allocated memory on success. NULL on failure.
 */
void * calloc(size_t nmemb, size_t size);


/*
 * reallocates the pointer with new size size and copies the old data to new
 * location.
 * params: pointer to reallocate. and new size.
 * returns: pointer to new allocated memory chunk.
 */
void * realloc(void *ptr, size_t size);


/* aligns memory.
 */
void * memalign(size_t alignment, size_t s);


/*
 * Prints malloc stats like number of free blocks, total number of memory
 * allocated.
 */
void malloc_stats();

void abortfn(enum mcheck_status status);

#endif
