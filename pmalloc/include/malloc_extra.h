#include "malloc.h"

#ifndef _MALLOC_EXTRA_H
#define _MALLOC_EXTRA_H 1

typedef struct block_info block_info;

// thread local
typedef struct thread_bins
{
    block_info *bin_8;
    block_info *bin_64;
    block_info *bin_512;
    block_info *bin_large;
} thread_bins;

/*
 * Inits context to work with pmmap.
 * Returns the thread ID. Use it to fetch the correct log.
 */
int pmalloc_init_thread_context(size_t logSize);

int pmalloc_get_tid();

void* pmalloc_get_thread_log();

thread_bins * pmalloc_get_bins();

void * pmalloc(size_t);
void pfree(void *p);
void * pcalloc(size_t nmemb, size_t size);
void * prealloc(void *ptr, size_t size);
void * pmemalign(size_t alignment, size_t s);
void pmalloc_stats();


#endif /* _MALLOC_EXTRA_H */