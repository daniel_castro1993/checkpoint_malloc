#include "malloc_extra.h"

#ifndef _PMMAP_H
#define _PMMAP_H 1

#define PMMAP_PATH_TO_PM      "/dev/shm/"
#define PMMAP_CONFIG_FILE     "pmalloc"
#define PMMAP_MALLOC_LOG_SIZE 4096
#define PMMAP_NONTX_LOG_SIZE  4096
#define PMMAP_MAX_LOGS        128

// number of words (8B) after the logBaseAddr where data is mantained
// TODO: use at least a cache line (8 x8B) to avoid TSX conflicts
#define PMMAP_LOG_PADDING 3 // minimum is 3 (size/start/next)

/* log entry: <addr(8B), val(8B)> | <endOfTX> | <-1(8B), mallocAddr> */
// if malloc is not page aligned, then is discarded (small mallocs)
typedef struct pmmap_config { // must fit in 1 page
  int32_t isExit;
  // Checkpointer changes this to (1<<31)|tid and requester changes to -1 afterwards
  int32_t isMalloc; // -1 == free, tid == request, (1<<31)|tid == requestDone for tid
  void *rootPtr;
  void *mallocLogPtr; // logs all the malloced pages
  void *logBasePtr[PMMAP_MAX_LOGS];
  void *nonTXLogBasePtr[PMMAP_MAX_LOGS];
  thread_bins bins[PMMAP_MAX_LOGS]; // malloc metadata
} pmmap_config;

typedef enum {
  PMMAP_ENTRY = 1,
  PMMAP_PAYLOAD = 2,
  PMMAP_END_OF_TX = 3,
  PMMAP_MALLOC = 4
} PMMAP_ENTRY_TYPE;

typedef union pmmap_log_entry {
  struct {
    uintptr_t addr;
    uintptr_t val;
  } entry;
  struct {
    uintptr_t addr;
    uintptr_t size;
  } malloc;
  struct {
    uintptr_t addr;
    uintptr_t size;
  } payload;
  uintptr_t endOfTX;
} pmmap_log_entry;

// callback that wait waits for more log (default just spins)
extern uintptr_t(*pmmap_log_wait)(void*);

extern int isCheckpointProcessAlive;

// pass MAP_PRIVATE for app or MAP_SHARED for the checkpointer
// returns a pointer where the rootPtr should be written,
// rootPtr is a pointer to a datascructure that tracks all
// the allocated memory
void ** pmmap_init(
  int mapPrivateOrShared, /* */
  uintptr_t **exitFlag,   /* */
  uintptr_t **mallocFlag  /* */
); // loads pages in pmalloc dir and returns rootPtr 

thread_bins * pmmap_get_bins(int id); // returns malloc metadata

// size_t is only used on creation (first usage)
void pmmap_free(void*);
void * pmmap_new_page(void*, size_t);
void * pmmap_log_get(int id, size_t, void **nonTXLogBasePtr); // returns pointer to beginning of the log
void * pmmap_malloc_log_get();
void *pmmap_log_alloc(size_t sizePages);

#define PMALLOC_WRITE_LOG_VAL(_entryAddr, _entryVal, _entry) \
  *((uintptr_t*)(_entryAddr)) = (_entry).entry.addr; \
  *((uintptr_t*)(_entryVal)) = (_entry).entry.val \
//

#define PMALLOC_WRITE_LOG_END_OF_TX(_entryMarker, _entry) \
  *((uintptr_t*)(_entryMarker)) = (_entry).endOfTX \
//

#define PMALLOC_WRITE_LOG_MALLOC(_entryMarker, _entryAddr, _mallocEntry) \
  *((uintptr_t*)(_entryMarker)) = ((uintptr_t)-1 << 62) | (_mallocEntry).malloc.size; \
  *((uintptr_t*)(_entryAddr)) = (_mallocEntry).malloc.addr \
//  

// TODO: flush log first then pointer
#define PMALLOC_WRITE_LOG_PAYLOAD(_logBase, _logAddr1, _logAddr2, _logSize, _logNext, _entry) ({ \
  uintptr_t _ptrSize = _entry.payload.size / sizeof(uintptr_t); \
  uintptr_t _remainderSize = _entry.payload.size % sizeof(uintptr_t); \
  uintptr_t _size = (_ptrSize + (_remainderSize > 0 ? 1 : 0)); \
  *((uintptr_t*)(_logAddr1)) = ((uintptr_t)-3 << 62) | (_entry).payload.size; \
  *((uintptr_t*)(_logAddr2)) = (_entry).payload.addr;  \
  if (_logNext + _size < _logSize) { \
    memcpy( \
      ((uintptr_t*)(_logBase) + PMMAP_LOG_PADDING + _logNext), \
      (void*)(_entry).payload.addr, \
      (_entry).payload.size \
    ); \
    _logNext = _logNext + _size; \
  } else { \
    uintptr_t _size2 = (_logNext + _size - _logSize) * sizeof(uintptr_t); \
    uintptr_t _size1 = (_entry).payload.size - _size2; \
    uintptr_t _extra = _size1 % sizeof(uintptr_t); \
    if (_extra > 0) { \
      _size1 += sizeof(uintptr_t) - _extra; \
      _size2 -= sizeof(uintptr_t) - _extra; \
    } \
    memcpy( \
      (uintptr_t*)(_logBase) + PMMAP_LOG_PADDING + _logNext, \
      (void*)(_entry).payload.addr, \
      _size1 \
    ); \
    memcpy( \
      (uintptr_t*)(_logBase) + PMMAP_LOG_PADDING, \
      (uintptr_t*)(_entry).payload.addr + _size1/sizeof(uintptr_t), \
      _size2 \
    ); \
    _logNext = _size2/sizeof(uintptr_t) + (_size2%sizeof(uintptr_t) > 0 ? 1 : 0); \
  } \
})

#define PMALLOC_READ_LOG_VAL(_entryAddr, _entryVal, _entry) \
  (_entry)->entry.addr = *((uintptr_t*)(_entryAddr)); \
  (_entry)->entry.val = *((uintptr_t*)(_entryVal)); \
  *((uintptr_t*)(_entry)->entry.addr) = (_entry)->entry.val \
//

#define PMALLOC_READ_LOG_END_OF_TX(_entryMarker, _entry) \
  (_entry)->endOfTX = *((uintptr_t*)(_entryMarker)) \
//

// new pages are intercepted from special malloc
#define PMALLOC_READ_LOG_MALLOC(_entryMarker, _entryAddr, _mallocEntry) \
  (_mallocEntry)->malloc.size = *((uintptr_t*)(_entryMarker)) & ~((uintptr_t)-1 << 62); \
  (_mallocEntry)->malloc.addr = *((uintptr_t*)(_entryAddr)); \
  pmmap_new_page((uintptr_t*)(_mallocEntry)->malloc.addr, (_mallocEntry)->malloc.size) \
//

// TODO: flush log first then pointer
#define PMALLOC_READ_LOG_PAYLOAD(_logBase, _logAddr1, _logAddr2, _logSize, _logPos, _entry) ({ \
  (_entry)->payload.size = *((uintptr_t*)(_logAddr1)) & ~((uintptr_t)-1 << 62); \
  uintptr_t _ptrSize = (_entry)->payload.size / sizeof(uintptr_t); \
  uintptr_t _remainderSize = (_entry)->payload.size % sizeof(uintptr_t); \
  uintptr_t _size = (_ptrSize + (_remainderSize > 0 ? 1 : 0)); \
  (_entry)->payload.addr = *((uintptr_t*)(_logAddr2));  \
  if (_logPos + _size < _logSize) { \
    memcpy( \
      (void*)(_entry)->payload.addr, \
      ((uintptr_t*)(_logBase) + PMMAP_LOG_PADDING + _logPos), \
      (_entry)->payload.size \
    ); \
    _logPos = _logPos + _size; \
  } else { \
    uintptr_t _size2 = (_logPos + _size - _logSize) * sizeof(uintptr_t); \
    uintptr_t _size1 = (_entry)->payload.size - _size2; \
    uintptr_t _extra = _size1 % sizeof(uintptr_t); \
    if (_extra > 0) { \
      _size1 += sizeof(uintptr_t) - _extra; \
      _size2 -= sizeof(uintptr_t) - _extra; \
    } \
    memcpy( \
      (void*)(_entry)->payload.addr, \
      (uintptr_t*)(_logBase) + PMMAP_LOG_PADDING + _logPos, \
      _size1 \
    ); \
    memcpy( \
      (uintptr_t*)(_entry)->payload.addr + _size1/sizeof(uintptr_t), \
      (uintptr_t*)(_logBase) + PMMAP_LOG_PADDING, \
      _size2 \
    ); \
    _logPos = _size2/sizeof(uintptr_t) + (_size2%sizeof(uintptr_t) > 0 ? 1 : 0); \
  } \
})

// TODO: the read also applies, we need an inspect next transaction on multiple logs

// returns logNext (DO NOT FORGET to call pmmap_log_metadata_write after and do the flushes)
uintptr_t pmmap_log_write(
  void *logAddr,         /* */
  uintptr_t logSize,     /* use pmmap_log_metadata_read to discover these */
  uintptr_t logStart,    /* */
  uintptr_t logNext,     /* */
  pmmap_log_entry entry, /* */
  PMMAP_ENTRY_TYPE type  /* */
);

// returns logStart (DO NOT FORGET to call pmmap_log_metadata_write after and do the flushes)
uintptr_t pmmap_log_read(
  void *logAddr,          /* */
  uintptr_t logSize,      /* use pmmap_log_metadata_read to discover these */
  uintptr_t logStart,     /* */
  uintptr_t logNext,      /* */
  pmmap_log_entry *entry, /* */
  PMMAP_ENTRY_TYPE *type  /* */
);

void pmmap_log_metadata_read(
  void *logAddr,        /* */
  uintptr_t *logSize,   /* */
  uintptr_t *logStart,  /* */
  uintptr_t *logNext    /* */
);

void pmmap_log_metadata_update_start(
  void *logAddr,       /* */
  uintptr_t logStart   /* */
);

void pmmap_log_metadata_update_next(
  void *logAddr,       /* */
  uintptr_t logNext    /* */
);

// TODO: flush range

#endif /* _PMMAP_H */
