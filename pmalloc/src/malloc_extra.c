/******************************************************************************
 * Implements malloc library in C.
 * This malloc implemtation has per thread bins.
 * bins are according to four sizes:
 * 8 bytes, 64 bytes, 512 bytes and greated than 512 bytes.
 *
 * memory for size > 512 bytes is allocated through mmap system call.
 * memory blocks are initialized lazily and are appended on free list after
 * free() call.
 *
 * Author: Savan Patel
 * Email : patel.sav@husky.neu.edu
 *
 * Edited by: Daniel Castro
 * Email: daniel.castro@ist.utl.pt
 *************************************************************************** */
#include "malloc_extra.h"
#include "pmmap.h"

#include <pthread.h>

#ifndef DISABLE_MALLOC_PROFILING
#define INC_STATS_COUNTER(_cntAddr, _inc) __sync_fetch_and_add((_cntAddr), (_inc))
#else
#define INC_STATS_COUNTER(_cntAddr, _inc) /* empty */
#endif

#define PMMAP_LOG_ACCESS(_addr, _val, _type) \
  if (threadLog) \
  { \
    while (1) { \
      logTmpPtr = pmmap_log_write(threadLog, logSize, logStart, logNext, \
        (pmmap_log_entry){ \
            .entry.addr = (uintptr_t)(_addr), \
            .entry.val = (uintptr_t)(_val) \
      }, _type); \
      if (logTmpPtr == -1) logStart = pmmap_log_wait(threadLog); \
      else break; \
    } \
    logNext = logTmpPtr; \
    logTmpPtr = -1; \
  } \
//

#define PMMAP_MALLOC_LOG_ACCESS(_addr, _val, _type) \
  if (threadMallocLog) \
  { \
    while (1) { \
      logTmpPtr = pmmap_log_write(threadMallocLog, logSize, logStart, logNext, \
        (pmmap_log_entry){ \
            .entry.addr = (uintptr_t)(_addr), \
            .entry.val = (uintptr_t)(_val) \
      }, _type); \
      if (logTmpPtr == -1) logStart = pmmap_log_wait(threadMallocLog); \
      else break; \
    } \
    logNext = logTmpPtr; \
    logTmpPtr = -1; \
  } \
//

/* struct to hold block metadata
 * size represents the free block's size in bytes.
 * next points to next free block.
 */
typedef struct block_info
{
   intptr_t size;
   struct block_info *next;
} block_info;

/*mutex for global heap.*/
pthread_mutex_t global_heap_mutex = PTHREAD_MUTEX_INITIALIZER;

/* mutex for updating the stats variables */
pthread_mutex_t stats_mutex = PTHREAD_MUTEX_INITIALIZER;

static unsigned long total_mmap_size_allocated = 0;
static unsigned long total_allocation_request = 0;
static unsigned long total_free_request = 0;
static unsigned long total_free_blocks = 0;
static unsigned long total_recycled_size = 0;

/* Four bins of size 8 byte, 64 byte, 512 byte and every thing else greater
 *  than 512 bytes.
 *
 * Each thread will have its own bin and storage arena.
 * Iniially all the bins are empty. The list gets build up on successive free
 * calls after malloc.
 */
static __thread thread_bins threadLocalBins = {
  .bin_8 = NULL,
  .bin_64 = NULL,
  .bin_512 = NULL,
  .bin_large = NULL
};
static __thread thread_bins *bins = NULL; // use the one from pmmap

static int nbThreads = 0;
static __thread int tid = -1;
static __thread void *threadLog = NULL;
static __thread void *threadMallocLog = NULL;


/*
 *  returns the pointer to elated bin based on the size.
 *  params: size of bin.
 *  returns: pointer to bin corresponding size.
 */
static inline block_info** get_bin(size_t size)
{
  switch(size)
  {
    case 8   : return &bins->bin_8;
    case 64  : return &bins->bin_64;
    case 512 : return &bins->bin_512;
    default  : return &bins->bin_large;
  }
}


// returns minimum 2
static inline uintptr_t FloorPowerOf2(uintptr_t n)
{
  uintptr_t ret;
  for (int i = 63; i != 1; ++i) {
    if ((ret = (n & (1 << i)))) return ret;
  }
  return 2;
}


/*
 * Finds best fit block from bin_large. On memory request > 512, first
 * it is checked if any of large free memory chunks fits to request.
 *
 * params: size of block to allocate.
 * returns: pointer to best fitting block, NULL on failure.
 */
static inline void * find_best_fit_from_bin_large(size_t size)
{
  block_info *b = bins->bin_large;
  block_info *best_fit = NULL;
  int min_fit_size = INT_MAX;
  void *ret = NULL;
  uintptr_t logSize, logStart, logNext, logTmpPtr = -1;

  while (b != NULL)
  {
    if (b->size >= size && b->size < min_fit_size)
    {
      best_fit = b;
      min_fit_size = b->size;
    }
    b = b->next;
  }

  /* If best fit found, update list */
  if (NULL != best_fit)
  {
    if (threadLog)
    {
      pmmap_log_metadata_read(threadLog, &logSize, &logStart, &logNext);
    }
    // if best_fit is first block.
    if (best_fit == bins->bin_large)
    {
      bins->bin_large = bins->bin_large->next;
      PMMAP_LOG_ACCESS(bins->bin_large, bins->bin_large->next, PMMAP_ENTRY);
      best_fit->next = NULL;
      PMMAP_LOG_ACCESS(best_fit->next, NULL, PMMAP_ENTRY);
      ret = (void *)((void *)best_fit + sizeof(block_info));
    }
    else
    {
      b = bins->bin_large;
      while (b != NULL && b->next != best_fit)
      {
        b = b->next;
      }
      if (b != NULL)
      {
        b->next = best_fit->next;
        PMMAP_LOG_ACCESS(b->next, best_fit->next, PMMAP_ENTRY);
      }
      best_fit->next = NULL;
      PMMAP_LOG_ACCESS(best_fit->next, NULL, PMMAP_ENTRY);
      ret = (void *)((void *)best_fit + sizeof(block_info));
    }
    if (threadLog)
    {
      pmmap_log_metadata_update_next(threadLog, logNext);
    }
  }
  return ret;
}


/*
 * maps new memory address using mmap system call for size
 * request > 512 bytes.
 * Requests kernel to map new memory at some place decided by kernel.
 * params: requested size in bytes.
 * returns: pointer to block allocated., NULL on failure.
 */
static inline void * mmap_new_memory(size_t size)
{
  int num_pages =
      ((size + sizeof(block_info) - 1)/sysconf(_SC_PAGESIZE)) + 1;
  int required_page_size = sysconf(_SC_PAGESIZE) * num_pages;
  uintptr_t logSize, logStart, logNext, logTmpPtr = -1;

  void *ret = mmap(NULL, // let kernel decide.
                    required_page_size,
                    PROT_READ | PROT_WRITE,
                    MAP_ANONYMOUS | MAP_PRIVATE, // TODO
                    -1, //no file descriptor
                    0); //offset.

  if (threadMallocLog) pmmap_log_metadata_read(threadMallocLog, &logSize, &logStart, &logNext);
  PMMAP_MALLOC_LOG_ACCESS(ret, required_page_size, PMMAP_MALLOC);

  // TODO: this info must be written to the log and replayed by the checkpointer
  block_info b;
  b.size = (required_page_size - sizeof(block_info));
  b.next = NULL;
  ret = memcpy(ret, &b, sizeof(block_info));

  PMMAP_MALLOC_LOG_ACCESS(ret, sizeof(block_info), PMMAP_PAYLOAD);
  if (threadMallocLog) pmmap_log_metadata_update_next(threadMallocLog, logNext);

  ret = ((char*)ret + sizeof(block_info));

  // update stats variables.
  INC_STATS_COUNTER(&total_mmap_size_allocated, required_page_size);

  return ret;
}


// tries to find a freed block
static inline void *heap_allocate(size_t size)
{
  block_info **bin = get_bin(size);
  void * ret = NULL;
  uintptr_t logSize, logStart, logNext, logTmpPtr = -1;
  /* reuse memory block from heap bins if available*/
  if (NULL != *bin)
  {
    // writes to bin AND to page
    block_info *p = *bin;
    *bin =  p->next;
    if (threadLog)
    {
      pmmap_log_metadata_read(threadLog, &logSize, &logStart, &logNext);
    }
    PMMAP_LOG_ACCESS(bin, p->next, PMMAP_ENTRY);
    p->next = NULL;
    PMMAP_LOG_ACCESS(p->next, NULL, PMMAP_ENTRY);
    if (threadLog)
    {
      pmmap_log_metadata_update_next(threadLog, logNext);
    }

    INC_STATS_COUNTER(&total_free_blocks, -1);
    ret = (void *)((char*)p + sizeof(block_info));
  }

  return ret;
}


/*
 * Allocates the memory.
 */
void* pmalloc(size_t size)
{
  INC_STATS_COUNTER(&total_allocation_request, 1);
  void * ret = NULL;
  if (size < 0)
  {
    perror("\n Invalid memory request.");
    return NULL;
  }

  if (bins == NULL) bins = &threadLocalBins;

  if (size <= 512)
  {
    size = (size <= 8) ? 8 : ((size <= 64) ? 64 : 512);
    ret = heap_allocate(size);
    INC_STATS_COUNTER(&total_recycled_size, size);
    if (ret != NULL) return ret; // found free block
  }

  if (NULL != bins->bin_large)
  {
    pthread_mutex_lock(&global_heap_mutex);
    ret = find_best_fit_from_bin_large(size);
    INC_STATS_COUNTER(&total_recycled_size, size);
    pthread_mutex_unlock(&global_heap_mutex);
  }

  /* either bin_large is empty or no best fit was found. */
  if (ret == NULL)
  {
    pthread_mutex_lock(&global_heap_mutex);
    ret = mmap_new_memory(size);
    pthread_mutex_unlock(&global_heap_mutex);
  }
  return ret;
}


/*
 * Free up the memory allocated at pointer p. It appends the block into free
 * list.
 * params: address to pointer to be freed.
 * returns: NONE.
 */
void pfree(void *p)
{
  //update stats variables.
  INC_STATS_COUNTER(&total_free_request, 1);
  INC_STATS_COUNTER(&total_free_blocks, 1);

  if (bins == NULL) bins = &threadLocalBins;

  if (NULL != p)
  {
    block_info *block  = (block_info *)(p - sizeof(block_info));
    // memset(p, '\0', block->size); // TODO: cleanup not needed 

    block_info **bin = get_bin(block->size);
    block_info *check_bin = *bin;

    // already freed?
    while (check_bin != NULL)
    {
      if (check_bin == block)
      {
        return;
      }
      check_bin = check_bin->next;
    }

    // attach as head to free list of corresponding bin.
    block->next = *bin;
    *bin = block;
  }
}


/*similar to calloc of glibc */
void *pcalloc(size_t nmemb, size_t size)
{
  void *p = malloc(nmemb * size);
  block_info *b = (block_info *)(p - sizeof(block_info));
  memset(p, '\0', b->size);
  return p;
}


/*
 * Allocate size of size bytes for nmemb. Initialize with null bytes.
 * params: total number of elements of size 'size' to be allocated.
 *         and size to allocate.
 * returns: pointer to allocated memory on success. NULL on failure.
 */void *prealloc(void *ptr, size_t size)
{
  if(NULL == ptr)
  {
    return malloc(size);
  }
  void *newptr = malloc(size);

  if(NULL == newptr)
  {
    return NULL;
  }
  block_info *old_block =
      (block_info *)((char*)ptr - sizeof(block_info));

  // TODO: this also needs to be logged
  memcpy(newptr, ptr, old_block->size);

  free(ptr);

  return newptr;
}


void *pmemalign(size_t alignment, size_t s)
{
  alignment = FloorPowerOf2(alignment);
  if (alignment < sizeof(block_info)) {
    alignment = sizeof(block_info); // must be base 2 (16B currently)
  }
  void *ret = malloc(s + alignment); // always wastes memory
  if (((uintptr_t)ret & (alignment - 1)) != 0) { // most likely
    // need alignment
    ret = (void*)(((uintptr_t)ret & ~(alignment - 1)) + alignment);
  }
  return ret;
}


void pmalloc_stats()
{
  printf("\n -- malloc stats--\n");
  printf("\n total_mmap_size_allocated  : %lu", total_mmap_size_allocated);
  printf("\n total_allocation_request   : %lu", total_allocation_request);
  printf("\n total_free_request         : %lu", total_free_request);
  printf("\n total_free_blocks          : %lu", total_free_blocks);
  printf("\n total_recycled_size        : %lu\n", total_recycled_size);
}


int pmalloc_init_thread_context(size_t logSize)
{
  tid = __sync_fetch_and_add(&nbThreads, 1);
  bins = pmmap_get_bins(tid);
  pmmap_log_get(tid, logSize, &threadLog);
  threadMallocLog = pmmap_malloc_log_get();
  return tid;
}

int pmalloc_get_tid()
{
  return tid;
}

void * pmalloc_get_thread_log()
{
  return threadLog;
}

thread_bins * pmalloc_get_bins()
{
  return bins;
}


/*
 * Fork hook. This will be called before fork happens.
 * The method holds lock so as to make sure none of the active threads
 * are holding lock and thus making sure, none of the threads are
 * in middle of malloc process.
 * later child hooks can reset the mutex to initial value.
 */
void prep_fork(void)
{
  // take lock before fork so as to make sure no other thread is
  // holding lock.
  pthread_mutex_lock(&global_heap_mutex);
}


/*
 * Since mutex is held by prep_fork method, it can safely be reset
 * in parent process.
 */
void parent_fork_handle(void)
{
  pthread_mutex_init(&global_heap_mutex, NULL);
}


/*
 * Since mutex is held by prep_fork method, it can safely be reset
 * in child process.
 */
void child_fork_handle(void)
{
  pthread_mutex_init(&global_heap_mutex, NULL);
}


/*
 * Global constructor to Initialize the fork hooks
 */
__attribute__((constructor)) void sharedLibConstructor(void)
{
  int ret =
      pthread_atfork(&prep_fork,
                     &parent_fork_handle,
                     &child_fork_handle);
  if (ret != 0)
  {
    perror("pthread_atfork() error [Call #1]. Malloc is now not fork safe.");
  }

  /*if(mcheck(NULL) != 0)
  {  TODO: mcheck implemtation.
      perror("\n mcheck failed");
  }*/

}

