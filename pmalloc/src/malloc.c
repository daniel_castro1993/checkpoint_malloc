/******************************************************************************
 * Implements malloc library in C.
 * This malloc implemtation has per thread bins.
 * bins are according to four sizes:
 * 8 bytes, 64 bytes, 512 bytes and greated than 512 bytes.
 *
 * memory for size > 512 bytes is allocated through mmap system call.
 * memory blocks are initialized lazily and are appended on free list after
 * free() call.
 *
 * Author: Savan Patel
 * Email : patel.sav@husky.neu.edu
 *
 * Edited by: Daniel Castro
 * Email: daniel.castro@ist.utl.pt
 *************************************************************************** */
#include "malloc.h"
#include "malloc_extra.h"
#include "pmmap.h"

#include <pthread.h>

/*
 * Allocates the memory.
 */
void* malloc(size_t size)
{
    return pmalloc(size);
}


/*
 * Free up the memory allocated at pointer p. It appends the block into free
 * list.
 * params: address to pointer to be freed.
 * returns: NONE.
 */
void free(void *p)
{
   pfree(p);
}


/*similar to calloc of glibc */
void *calloc(size_t nmemb, size_t size)
{
    return pcalloc(nmemb, size);
}


void *realloc(void *ptr, size_t size)
{
    return prealloc(ptr, size);
}

void *memalign(size_t alignment, size_t s)
{
    return pmemalign(alignment, s); // TODO: not available
}


void malloc_stats()
{
    pmalloc_stats();
}
