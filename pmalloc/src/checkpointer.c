#define _GNU_SOURCE 
#include "pmmap.h"

#include <signal.h>
#include <stdlib.h>
#include <pthread.h>

static pthread_mutex_t malloc_lock;

void apply_malloc_log()
{
  pthread_mutex_lock(&malloc_lock);
  void *logAddr = pmmap_malloc_log_get();
  uintptr_t logSize, logStart, logNext, logTmpPtr = -1;
  PMMAP_ENTRY_TYPE type;
  pmmap_log_entry entry;
  pmmap_log_metadata_read(logAddr, &logSize, &logStart, &logNext);
  while (logStart != logNext)
  {
    logTmpPtr = pmmap_log_read(logAddr, logSize, logStart, logNext, &entry, &type);
    if (logTmpPtr == -1) break;
    printf("Addr=%p val=%lu\n", entry.entry.addr, entry.entry.val);
    logStart = logTmpPtr;
  }
  pmmap_log_metadata_update_start(logAddr, logStart);
  pthread_mutex_unlock(&malloc_lock);
}

void sighandler(int signum, siginfo_t *siginfo, void *context)  
{
  apply_malloc_log(); // TODO: what if it is a bug?
}

void apply_log(void *logAddr)
{
  uintptr_t logSize, logStart, logNext, logTmpPtr = -1;
  PMMAP_ENTRY_TYPE type;
  pmmap_log_entry entry;
  __atomic_thread_fence(__ATOMIC_ACQUIRE);
  pmmap_log_metadata_read(logAddr, &logSize, &logStart, &logNext);
  while (logStart != logNext)
  {
    logTmpPtr = pmmap_log_read(logAddr, logSize, logStart, logNext, &entry, &type);
    if (logTmpPtr == -1) break;
    printf("Addr=%p val=%lu\n", entry.entry.addr, entry.entry.val);
    logStart = logTmpPtr;
  }
  pmmap_log_metadata_update_start(logAddr, logStart);
  __atomic_thread_fence(__ATOMIC_RELEASE);
}

int main(int argc, char **argv)
{
  // TODO: argument == number of logs to look into

  void *rootPtr;
  int32_t *isExit;
  int32_t *isMalloc;
  struct sigaction trap;
  void * logAddr;

  rootPtr = pmmap_init(MAP_SHARED, &isExit, &isMalloc);
  pmmap_log_get(0, 4096, &logAddr);
  printf("rootPtr = %p logSize = %lu\n", rootPtr, *((uintptr_t*)logAddr));

  pthread_mutex_init(&malloc_lock, NULL);

  memset(&trap, 0x00, sizeof(trap));

  trap.sa_flags = SA_SIGINFO;
  trap.sa_sigaction = sighandler;
  sigaction(SIGSEGV, &trap, NULL);

  while (!*isExit)
  {
    if ()
    apply_log(logAddr);
    pthread_yield();
    usleep(1000);
  }

  pthread_mutex_destroy(&malloc_lock);
  return EXIT_SUCCESS;
}
