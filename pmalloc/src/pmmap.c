/**
 * In this implementations there is only 1 VAS.
 * Loading any application loads all pages.
 **/

#include "pmmap.h"
#include <dirent.h> 
#include <pthread.h>
#include <sys/mman.h>
#include <fcntl.h>

static pmmap_config *config = NULL;

static uintptr_t log_wait(void *logAddr)
{
  uintptr_t logSize, logStart, logNext;
  pmmap_log_metadata_read(logAddr, &logSize, &logStart, &logNext);
  // TODO: spin while no space (some constant)
  return logStart;
}
uintptr_t(*pmmap_log_wait)(void*) = log_wait;

static int startsWith(char *pre, char *str)
{
  size_t lenpre = strlen(pre),
          lenstr = strlen(str);
  return lenstr < lenpre ? 0 : memcmp(pre, str, lenpre) == 0;
}

static uintptr_t discover_config_page_addr()
{
  struct dirent *de;  // Pointer for directory entry
  uintptr_t ret = 0;
  size_t pageSize;
  
  // opendir() returns a pointer of DIR type.  
  DIR *dr = opendir(PMMAP_PATH_TO_PM); 

  if (dr == NULL)  // opendir returns NULL if couldn't open directory 
  {
    perror("dir does not exist");
    return ret; 
  } 

  while ((de = readdir(dr)) != NULL) {
    if (startsWith(PMMAP_CONFIG_FILE, de->d_name)) {
      sscanf(de->d_name, PMMAP_CONFIG_FILE "_%p_%zu", (void **)&ret, &pageSize);
      break;
    }
  }

  closedir(dr);
  return ret;
}

static void load_all_pages(int mmapFlag)
{
  struct dirent *de;  // Pointer for directory entry
  
  // opendir() returns a pointer of DIR type.  
  DIR *dr = opendir(PMMAP_PATH_TO_PM); 

  if (dr == NULL)  // opendir returns NULL if couldn't open directory 
  {
    perror("dir does not exist");
    return; 
  } 

  while ((de = readdir(dr)) != NULL) {
    uintptr_t addr = 0;
    size_t pageSize = 0;
    char map_name[512];
    int pm_fd;

    if (startsWith("page_", de->d_name))
    {
      sprintf(map_name, "%s", de->d_name);
      pm_fd = shm_open(map_name, O_RDWR, S_IRUSR | S_IWUSR);
      if (pm_fd < 0) perror("load_all_pages: pm_fd");
      sscanf(de->d_name, "page_%p_%zu", (void **)&addr, &pageSize);
      if (mmap((void*)addr, pageSize,
        PROT_READ|PROT_WRITE,
        MAP_FIXED|mmapFlag,
        pm_fd, 0
      ) == NULL) perror("load_all_pages: page mmap");
      close(pm_fd);
    }
    else if (startsWith("log_", de->d_name))
    {
      sprintf(map_name, "%s", de->d_name);
      pm_fd = shm_open(map_name, O_RDWR, S_IRUSR | S_IWUSR);
      if (pm_fd < 0) perror("load_all_pages: pm_fd");
      sscanf(de->d_name, "log_%p_%zu", (void **)&addr, &pageSize);
      if (mmap((void*)addr, pageSize,
        PROT_READ|PROT_WRITE,
        MAP_FIXED|MAP_SHARED,
        pm_fd, 0
      ) == NULL) perror("load_all_pages: log mmap");
      close(pm_fd);
    }
  }

  closedir(dr);
}  

void ** pmmap_init(int mmapFlag, int32_t **exitFlag, int32_t **mallocFlag)
{
  char map_name[512];
  int pm_fd;
  void *ret = NULL;
  int firstTime = 0;
  uintptr_t configPageAddr = 0;
  size_t configSize = sizeof(pmmap_config);

  if (config != NULL) return config->rootPtr;

  configPageAddr = discover_config_page_addr();
  if (configPageAddr == 0)
  {
    ret = mmap(NULL, configSize,
      PROT_READ|PROT_WRITE,
      MAP_ANONYMOUS|MAP_SHARED,
      -1, 0
    );
    if (ret == (void*)-1) perror("init pmmap: mmap");
    munmap(ret, configSize); // frees the mapping
    configPageAddr = (uintptr_t)ret;
    snprintf(map_name, 512, PMMAP_CONFIG_FILE "_%p_%zu", (void*)ret, configSize);
    pm_fd = shm_open(map_name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    if (pm_fd < 0) perror("init pmmap: pm_fd");
    if (ftruncate(pm_fd, configSize) == -1)  perror("init pmmap: ftruncate");
    // acquires the mapping again
    ret = mmap((void*)configPageAddr, configSize,
      PROT_READ|PROT_WRITE,
      MAP_FIXED|MAP_SHARED,
      pm_fd, 0
    );
    if ((uintptr_t)ret != configPageAddr) perror("WRONG MAPPING");
    config = (pmmap_config*)ret;
    memset(config, 0, configSize); // makes sure it is clean
    // allocates malloc log
    config->mallocLogPtr = pmmap_log_alloc(PMMAP_MALLOC_LOG_SIZE);
    *((uintptr_t*)config->mallocLogPtr    ) = PMMAP_MALLOC_LOG_SIZE / sizeof(uintptr_t) - PMMAP_LOG_PADDING; // log size
    *((uintptr_t*)config->mallocLogPtr + 1) = 0; // log start
    *((uintptr_t*)config->mallocLogPtr + 2) = 0; // log next
  }
  else
  {
    snprintf(map_name, 512, PMMAP_CONFIG_FILE "_%p_%zu", (void*)configPageAddr, configSize);
    pm_fd = shm_open(map_name, O_RDWR, S_IRUSR | S_IWUSR);
    if (pm_fd < 0) perror("init pmmap: pm_fd");
    ret = mmap((void*)configPageAddr, configSize,
      PROT_READ|PROT_WRITE,
      MAP_FIXED|MAP_SHARED,
      pm_fd, 0
    );
    load_all_pages(mmapFlag);
    config = (pmmap_config*)ret;
  }

  close(pm_fd);
  config->isExit = 0;
  if (exitFlag != NULL) *exitFlag = &config->isExit;
  if (mallocFlag != NULL) *mallocFlag = &config->isMalloc;
  return &(config->rootPtr);
}

thread_bins * pmmap_get_bins(int id)
{
  if (id >= PMMAP_MAX_LOGS) return NULL;
  return &(config->bins[id]);
}

void * pmmap_log_get(int id, size_t logSize, void **nonTXLogBasePtr)
{
  if (id >= PMMAP_MAX_LOGS) return NULL;
  if (config->logBasePtr[id] == NULL) {
    /* *** TX log *** */
    config->logBasePtr[id] = pmmap_log_alloc(logSize);
    *((uintptr_t*)config->logBasePtr[id]    ) = logSize / sizeof(uintptr_t) - PMMAP_LOG_PADDING; // log size
    *((uintptr_t*)config->logBasePtr[id] + 1) = 0; // log start
    *((uintptr_t*)config->logBasePtr[id] + 2) = 0; // log next
    /* *** non-TX log *** */
    config->nonTXLogBasePtr[id] = pmmap_log_alloc(PMMAP_NONTX_LOG_SIZE);
    *((uintptr_t*)config->nonTXLogBasePtr[id]    ) = PMMAP_NONTX_LOG_SIZE / sizeof(uintptr_t) - PMMAP_LOG_PADDING; // log size
    *((uintptr_t*)config->nonTXLogBasePtr[id] + 1) = 0; // log start
    *((uintptr_t*)config->nonTXLogBasePtr[id] + 2) = 0; // log next
  }
  if (nonTXLogBasePtr != NULL) *nonTXLogBasePtr = config->nonTXLogBasePtr[id];
  return config->logBasePtr[id];
}

void * pmmap_malloc_log_get()
{
  return config->mallocLogPtr;
}

void pmmap_free(void *addr)
{
  // assuming _SC_PAGESIZE power2
  uintptr_t addrInt = (uintptr_t)addr & (~(sysconf(_SC_PAGESIZE) - 1));
  char map_name[512];

  snprintf(map_name, 512, "page_%p", (void*)addrInt);
  if (shm_unlink(map_name))
  {
    perror("free mmap");
  }
}

/* size must be multiple of sysconf(_SC_PAGESIZE) */
void *pmmap_new_page(void *addrBase, size_t sizePages)
{
  // assuming _SC_PAGESIZE power2
  uintptr_t addrInt = (uintptr_t)addrBase & (~(sysconf(_SC_PAGESIZE) - 1));
  char map_name[512];
  int pm_fd;
  void *ret = NULL;

  snprintf(map_name, 512, "page_%p_%zu", (void*)addrInt, sizePages);

  pm_fd = shm_open(map_name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
  if (pm_fd < 0)
  {
    perror("alloc mmap: pm_fd");
  }
  if (ftruncate(pm_fd, sizePages) == -1)
  {
    perror("alloc mmap: ftruncate"); // allocates PM
  }

  ret = mmap((void*)addrInt, sizePages,
    PROT_READ|PROT_WRITE,
    MAP_FIXED|MAP_SHARED,
    pm_fd, 0
  );
  if (ret == NULL)
  {
    perror("alloc mmap: ret addr");
  }

  close(pm_fd);
  return ret;
}

/* size must be multiple of sysconf(_SC_PAGESIZE) */
void *pmmap_log_alloc(size_t sizePages)
{
  // assuming _SC_PAGESIZE power2
  char map_name[512];
  int pm_fd;
  void *ret = NULL;

  ret = mmap(NULL, sizePages,
    PROT_READ|PROT_WRITE,
    MAP_ANONYMOUS|MAP_SHARED,
    -1, 0
  );
  if (ret == (void*)-1) perror("pmmap_log_alloc mmap");
  if (munmap(ret, sizePages)) perror("pmmap_log_alloc unmmap");

  snprintf(map_name, 512, "log_%p_%zu", ret, sizePages);
  pm_fd = shm_open(map_name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
  if (pm_fd == -1) perror("alloc mmap: pm_fd");
  if (ftruncate(pm_fd, sizePages)) perror("alloc mmap: ftruncate");

  ret = mmap(ret, sizePages,
    PROT_READ|PROT_WRITE,
    MAP_FIXED|MAP_SHARED,
    pm_fd, 0
  );

  if (ret == (void*)-1) perror("pmmap_log_alloc 2nd mmap");

  close(pm_fd);
  return ret;
}

uintptr_t pmmap_log_write(void *logAddr, uintptr_t logSize, uintptr_t logStart, uintptr_t logNext, pmmap_log_entry entry, PMMAP_ENTRY_TYPE type)
{
  void *entryAddr1;
  void *entryAddr2;

  // must write in logNext, may write +1, or a larger payload
  // cannot completelly fill the log, else startPtr == nextPtr == empty
  if ((logNext + 1) % logSize == logStart || (logNext + 2) % logSize == logStart)
    return -1; // not enough space
  
  entryAddr1 = (void*)((uintptr_t*)logAddr + PMMAP_LOG_PADDING + logNext);
  if (logNext < logSize - 1) {
    entryAddr2 = (void*)((uintptr_t*)logAddr + PMMAP_LOG_PADDING + logNext + 1);
  } else {
    entryAddr2 = (void*)((uintptr_t*)logAddr + PMMAP_LOG_PADDING);
  }

  if (type == PMMAP_ENTRY)
  {
    PMALLOC_WRITE_LOG_VAL(entryAddr1, entryAddr2, entry);
    logNext = logNext < logSize - 2 ? logNext + 2 : (logNext < logSize - 1 ? 0 : 1);
  }
  else if (type == PMMAP_END_OF_TX)
  {
    PMALLOC_WRITE_LOG_END_OF_TX(entryAddr1, entry);
    logNext = logNext < logSize - 1 ? logNext + 1 : 0;
  }
  else if (type == PMMAP_PAYLOAD)
  {
    uintptr_t bufSize = entry.payload.size/sizeof(uintptr_t) + (entry.payload.size%sizeof(uintptr_t) > 0 ? 1 : 0);
    logNext = logNext < logSize - 2 ? logNext + 2 : (logNext < logSize - 1 ? 0 : 1);
    if ((logNext + bufSize + 1 > logSize && (logNext + bufSize + 1) % logSize > logStart)
        || (logNext < logStart && logNext + bufSize + 1 > logStart)) {
      return -1;
    }
    PMALLOC_WRITE_LOG_PAYLOAD(logAddr, entryAddr1, entryAddr2, logSize, logNext, entry);
  }
  else if (type == PMMAP_MALLOC)
  {
    PMALLOC_WRITE_LOG_MALLOC(entryAddr1, entryAddr2, entry);
    logNext = logNext < logSize - 2 ? logNext + 2 : (logNext < logSize - 1 ? 0 : 1);
  }
  else return -1; // invalid type

  return logNext;
}

uintptr_t pmmap_log_read(void *logAddr, uintptr_t logSize, uintptr_t logStart, uintptr_t logNext, pmmap_log_entry *entry, PMMAP_ENTRY_TYPE *type)
{
  void *entryAddr1;
  uintptr_t entryVal1;
  void *entryAddr2;
  const uintptr_t bitsMalloc  = ((uintptr_t)-1 << 62); // 0xC000 ...
  const uintptr_t bitsEndTX   = ((uintptr_t)-1 << 63); // 0x8000 ...
  const uintptr_t bitsPayload = ((uintptr_t)-3 << 62); // 0x4000 ...
  PMMAP_ENTRY_TYPE ret;

  // must write in logNext, may write +1, or a larger payload
  // cannot completelly fill the log, else startPtr == nextPtr == empty
  if (logStart == logNext)
    return -1; // log is empty
  
  entryAddr1 = (void*)((uintptr_t*)logAddr + PMMAP_LOG_PADDING + logStart);
  if (logStart < logSize - 1) {
    entryAddr2 = (void*)((uintptr_t*)logAddr + PMMAP_LOG_PADDING + logStart + 1);
  } else {
    entryAddr2 = (void*)((uintptr_t*)logAddr + PMMAP_LOG_PADDING);
  }

  entryVal1 = *((uintptr_t*)entryAddr1);
  if ((entryVal1 & bitsMalloc) == bitsMalloc) // this must come first (2 bits on)
    ret = PMMAP_MALLOC;
  else if ((entryVal1 & bitsEndTX) == bitsEndTX)
    ret = PMMAP_END_OF_TX;
  else if ((entryVal1 & bitsPayload) == bitsPayload)
    ret = PMMAP_PAYLOAD;
  else // it is a <key, val>
    ret = PMMAP_ENTRY;

  if (ret == PMMAP_ENTRY)
  {
    PMALLOC_READ_LOG_VAL(entryAddr1, entryAddr2, entry);
    logStart = logStart < logSize - 2 ? logStart + 2 : (logStart < logSize - 1 ? 0 : 1);
  }
  else if (ret == PMMAP_END_OF_TX)
  {
    PMALLOC_READ_LOG_END_OF_TX(entryAddr1, entry);
    logStart = logStart < logSize - 1 ? logStart + 1 : 0;
  }
  else if (ret == PMMAP_PAYLOAD)
  {
    entry->payload.size = (entryVal1 & (~bitsPayload));
    uintptr_t bufSize = entry->payload.size/sizeof(uintptr_t) + (entry->payload.size%sizeof(uintptr_t) > 0 ? 1 : 0);
    logStart = logStart < logSize - 2 ? logStart + 2 : (logStart < logSize - 1 ? 0 : 1);
    if ((logStart + bufSize + 1 > logSize && (logStart + bufSize + 1) % logSize > logNext)
        || (logStart < logNext && logStart + bufSize + 1 > logNext)) {
      return -1;
    }
    PMALLOC_READ_LOG_PAYLOAD(logAddr, entryAddr1, entryAddr2, logSize, logStart, entry);
  }
  else if (ret == PMMAP_MALLOC)
  {
    PMALLOC_READ_LOG_MALLOC(entryAddr1, entryAddr2, entry);
    logStart = logStart < logSize - 2 ? logStart + 2 : (logStart < logSize - 1 ? 0 : 1);
  }
  else return -1; // invalid type

  if (type != NULL) *type = ret;
  return logStart;
}

void pmmap_log_metadata_read(void *logAddr, uintptr_t *logSize, uintptr_t *logStart, uintptr_t *logNext)
{
  uintptr_t *logSizePtr  = ((uintptr_t*)logAddr    );
  uintptr_t *logStartPtr = ((uintptr_t*)logAddr + 1);
  uintptr_t *logNextPtr  = ((uintptr_t*)logAddr + 2);
  *logSize  = *logSizePtr;
  *logStart = *logStartPtr;
  *logNext  = *logNextPtr;
}

// size cannot change
void pmmap_log_metadata_update_start(void *logAddr, uintptr_t logStart)
{
  uintptr_t *logStartPtr = ((uintptr_t*)logAddr + 1);
  if (logStart != -1) *logStartPtr = logStart;
}

// size cannot change
void pmmap_log_metadata_update_next(void *logAddr, uintptr_t logNext)
{
  uintptr_t *logNextPtr  = ((uintptr_t*)logAddr + 2);
  if (logNext != -1) *logNextPtr = logNext; 
}
