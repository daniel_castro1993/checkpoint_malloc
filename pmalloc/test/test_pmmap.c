#include "pmmap.h"

#include <stdlib.h>

int main(int argc, char **argv)
{
  // TODO: argument == number of logs to look into

  void *rootPtr;
  rootPtr = pmmap_init(MAP_SHARED, NULL);
  void * logAddr = pmmap_log_get(0, 88, NULL);
  uintptr_t bufAligned[2] = { 1, 2 };
  char bufDisaligned[7] = { 0, 1, 2, 3, 4, 5, 6};
  uintptr_t writeLocation;
  uintptr_t logSize;
  uintptr_t logStart = 0;
  uintptr_t logNext = 0;
  pmmap_log_entry read_log;
  PMMAP_ENTRY_TYPE type;

  printf("rootPtr = %p logSize = %lu\n", rootPtr, *((uintptr_t*)logAddr));

  logStart = logNext = 0;
  pmmap_log_metadata_update_start(logAddr, logStart);
  pmmap_log_metadata_update_next(logAddr, logNext);
  pmmap_log_metadata_read(logAddr, &logSize, &logStart, &logNext);
  printf("logSize = %lu logStart = %lu logNext = %lu\n", logSize, logStart, logNext);

  int ret;

  // 16B
  printf(" --- Test writting 1 entry --- \n", ret);
  logNext = ret = pmmap_log_write(logAddr, logSize, logStart, logNext, (pmmap_log_entry){
    .entry.addr = (uintptr_t)&writeLocation,
    .entry.val = (uintptr_t)1
  }, PMMAP_ENTRY);
  pmmap_log_metadata_update_next(logAddr, logNext);
  printf("write 1 ret=%i\n", ret);
  // 32B
  logNext = ret = pmmap_log_write(logAddr, logSize, logStart, logNext, (pmmap_log_entry){
    .entry.addr = (uintptr_t)&writeLocation,
    .entry.val = (uintptr_t)2
  }, PMMAP_ENTRY);
  pmmap_log_metadata_update_next(logAddr, logNext);
  printf("write 2 ret=%i\n", ret);
  // 48B 
  logNext = ret = pmmap_log_write(logAddr, logSize, logStart, logNext, (pmmap_log_entry){
    .entry.addr = (uintptr_t)&writeLocation,
    .entry.val = (uintptr_t)3
  }, PMMAP_ENTRY);
  pmmap_log_metadata_update_next(logAddr, logNext);
  printf("write 3 ret=%i\n", ret);
  // 64B --> full (cannot write this because it would put nextPtr == startPtr)
  ret = pmmap_log_write(logAddr, logSize, logStart, logNext, (pmmap_log_entry){
    .entry.addr = (uintptr_t)&writeLocation,
    .entry.val = (uintptr_t)4
  }, PMMAP_ENTRY);
  printf("write 4 ret=%i\n", ret); // should fail

  printf(" --- Test reading the log (apply) --- \n", ret);
  logStart = ret = pmmap_log_read(logAddr, logSize, logStart, logNext,
    &read_log, &type);
  pmmap_log_metadata_update_start(logAddr, logStart);
  printf("read 1 ret=%i addr=%p(?==%p) valInAddr=%lu type=%i\n",
    ret, read_log.entry.addr, &writeLocation, writeLocation, type);
  logStart = ret = pmmap_log_read(logAddr, logSize, logStart, logNext,
    &read_log, &type);
  pmmap_log_metadata_update_start(logAddr, logStart);
  printf("read 2 ret=%i addr=%p(?==%p) valInAddr=%lu type=%i\n",
    ret, read_log.entry.addr, &writeLocation, writeLocation, type);
  logStart = ret = pmmap_log_read(logAddr, logSize, logStart, logNext,
    &read_log, &type);
  pmmap_log_metadata_update_start(logAddr, logStart);
  printf("read 3 ret=%i addr=%p(?==%p) valInAddr=%lu type=%i\n",
    ret, read_log.entry.addr, &writeLocation, writeLocation, type);
  printf("logSize = %lu logStart = %lu logNext = %lu\n", logSize, logStart, logNext);
  logNext = ret = pmmap_log_write(logAddr, logSize, logStart, logNext, (pmmap_log_entry){
    .entry.addr = (uintptr_t)&writeLocation,
    .entry.val = (uintptr_t)4
  }, PMMAP_ENTRY);
  pmmap_log_metadata_update_next(logAddr, logNext);
  printf("write 4 ret=%i\n", ret);

  // clean the log
  logStart = logNext = 0;
  pmmap_log_metadata_update_start(logAddr, logStart);
  pmmap_log_metadata_update_next(logAddr, logNext);

  printf(" --- Test writting payload --- \n", ret);
  logNext = ret = pmmap_log_write(logAddr, logSize, logStart, logNext, (pmmap_log_entry){
    .payload.addr = (uintptr_t)&bufAligned,
    .payload.size = (uintptr_t)sizeof(bufAligned)
  }, PMMAP_PAYLOAD);
  pmmap_log_metadata_update_next(logAddr, logNext);
  printf("1st write payload with size %zu ret=%i\n", sizeof(bufAligned), ret);
  // can't write not enough space
  logNext = ret = pmmap_log_write(logAddr, logSize, logStart, logNext, (pmmap_log_entry){
    .payload.addr = (uintptr_t)&bufAligned,
    .payload.size = (uintptr_t)sizeof(bufAligned)
  }, PMMAP_PAYLOAD);
  pmmap_log_metadata_update_next(logAddr, logNext);
  printf("2nd write payload with size %zu ret=%i\n", sizeof(bufAligned), ret);

  // clean the log
  logStart = logNext = 7;
  pmmap_log_metadata_update_start(logAddr, logStart);
  pmmap_log_metadata_update_next(logAddr, logNext);

  // same with disaligned log
  logNext = ret = pmmap_log_write(logAddr, logSize, logStart, logNext, (pmmap_log_entry){
    .payload.addr = (uintptr_t)&bufDisaligned,
    .payload.size = (uintptr_t)sizeof(bufDisaligned)
  }, PMMAP_PAYLOAD);
  pmmap_log_metadata_update_next(logAddr, logNext);
  printf("1st write payload with size %zu ret=%i\n", sizeof(bufDisaligned), ret);
  logNext = ret = pmmap_log_write(logAddr, logSize, logStart, logNext, (pmmap_log_entry){
    .payload.addr = (uintptr_t)&bufDisaligned,
    .payload.size = (uintptr_t)sizeof(bufDisaligned)
  }, PMMAP_PAYLOAD);
  pmmap_log_metadata_update_next(logAddr, logNext);
  printf("2nd write payload with size %zu ret=%i\n", sizeof(bufDisaligned), ret);
  // can't write not enough space
  ret = pmmap_log_write(logAddr, logSize, logStart, logNext, (pmmap_log_entry){
    .payload.addr = (uintptr_t)&bufDisaligned,
    .payload.size = (uintptr_t)sizeof(bufDisaligned)
  }, PMMAP_PAYLOAD);
  // TODO: if it returns -1 does not set the logNext
  printf("3rd write payload with size %zu ret=%i\n", sizeof(bufDisaligned), ret);

  printf(" --- Test reading the payload --- \n", ret);
  printf("logSize = %lu logStart = %lu logNext = %lu\n", logSize, logStart, logNext);
  bufDisaligned[3] = 0;
  logStart = ret = pmmap_log_read(logAddr, logSize, logStart, logNext,
    &read_log, &type);
  pmmap_log_metadata_update_start(logAddr, logStart);
  printf("read 1 ret=%i addr=%p(?==%p) someValInBuf=%i(?==3) type=%i\n",
  ret, read_log.entry.addr, &bufDisaligned, bufDisaligned[3], type);
  logStart = ret = pmmap_log_read(logAddr, logSize, logStart, logNext,
    &read_log, &type);
  pmmap_log_metadata_update_start(logAddr, logStart);
  printf("read 2 ret=%i addr=%p(?==%p) someValInBuf=%i(?==3) type=%i\n",
    ret, read_log.entry.addr, &bufDisaligned, bufDisaligned[3], type);
  printf("logSize = %lu logStart = %lu logNext = %lu\n", logSize, logStart, logNext);
  logNext = ret = pmmap_log_write(logAddr, logSize, logStart, logNext, (pmmap_log_entry){
    .payload.addr = (uintptr_t)&bufDisaligned,
    .payload.size = (uintptr_t)sizeof(bufDisaligned)
  }, PMMAP_PAYLOAD);
  pmmap_log_metadata_update_next(logAddr, logNext);
  printf("3rd write payload with size %zu ret=%i\n", sizeof(bufDisaligned), ret);

  return EXIT_SUCCESS;
}
