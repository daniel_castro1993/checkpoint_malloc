
#include <stdio.h> 
#include <dirent.h> 
#include <sys/types.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
  
int main(void) 
{ 
    struct dirent *de;  // Pointer for directory entry 
  
    // opendir() returns a pointer of DIR type.  
    DIR *dr = opendir("."); 
  
    if (dr == NULL)  // opendir returns NULL if couldn't open directory 
    { 
        printf("Could not open current directory" ); 
        return 0; 
    } 
  
    // Refer http://pubs.opengroup.org/onlinepubs/7990989775/xsh/readdir.html 
    // for readdir() 
    while ((de = readdir(dr)) != NULL) 
            printf("%s\n", de->d_name);
  
    closedir(dr);

    int fd = shm_open("/someFile0x7ffff7fcb56a", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    if (fd == -1) perror("open file");
    if (ftruncate(fd, sysconf(_SC_PAGESIZE)) == -1)  perror("init pmmap: ftruncate");

    return 0; 
} 
