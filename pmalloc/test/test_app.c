#include "pmmap.h"
#include "malloc_extra.h"

#include <stdlib.h>

// simple application with 1 persistent counter
int main(int argc, char **argv)
{
  // TODO: argument == number of logs to look into

  uintptr_t logSize, logStart, logNext, logTmpPtr = -1;
  void **rootPtr;
  int *counter;
  rootPtr = pmmap_init(MAP_PRIVATE, NULL); // care with SHARED|PRIVATE

  int tid = pmalloc_init_thread_context(4096);
  void * logAddr;
  
  pmmap_log_get(tid, 4096, &logAddr);

  printf("rootPtr = %p logSize = %lu\n", rootPtr, *((uintptr_t*)logAddr));

  if (*rootPtr == NULL) {
    *rootPtr = pmalloc(sizeof(int)); // must be the special malloc
  }
  counter = (int*)*rootPtr;

  printf(" --- \ncounter changed %i -> %i\n --- \n", *counter, *counter + 1);
  ++(*counter);

  __atomic_thread_fence(__ATOMIC_ACQUIRE);
  pmmap_log_metadata_read(logAddr, &logSize, &logStart, &logNext);
  while (logTmpPtr == -1)
  {
    logTmpPtr = pmmap_log_write(logAddr, logSize, logStart, logNext,
      (pmmap_log_entry) {
        .entry.addr = (uintptr_t)counter,
        .entry.val = (uintptr_t)*counter
      }, PMMAP_ENTRY);
    logStart = pmmap_log_wait(logAddr);
  }
  logNext = logTmpPtr;
  pmmap_log_metadata_update_next(logAddr, logNext);
  __atomic_thread_fence(__ATOMIC_RELEASE);

  return EXIT_SUCCESS;
}
