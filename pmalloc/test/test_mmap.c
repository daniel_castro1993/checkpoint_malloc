#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

#define PAGE_SIZE 4096

uintptr_t addr1 = 0x10000000;
uintptr_t addr2 = 0x20000000;
int *ptr;
int fd;

// need memory mapped file system

static void HandleSignal(int sig, siginfo_t *info, void *extra)
{
  static int countSigsegv = 0;
  if (sig == SIGSEGV) {
    printf("Faulty address in %p\n", info->si_addr);
    mmap(info->si_addr, PAGE_SIZE, PROT_READ|PROT_WRITE, MAP_FIXED|MAP_PRIVATE, fd, 0);
    if (countSigsegv > 1) {
      shm_unlink("/myregion");
      printf("remap did not work\n");
      struct sigaction sa;
      sa.sa_handler = SIG_DFL;
      sigemptyset(&sa.sa_mask);
      sa.sa_flags = SA_RESTART | SA_SIGINFO;
      if (sigaction(SIGSEGV, &sa, NULL) < 0) {
        perror("sigaction(SIGSEGV)");
      }
    }
    countSigsegv++;
  }
}

int main()
{
#ifdef __APPLE__
  unlink("/tmp/myregion"); // TODO: must be a DAX mounted fs
  perror("unlink");
  fd = open("/tmp/myregion", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
  perror("open");
  if (ftruncate(fd, PAGE_SIZE) == -1) {
    perror("ftruncate");
    return EXIT_FAILURE;
  }
#else
  shm_unlink("/myregion");
  perror("shm_unlink");
  fd = shm_open("/myregion", O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
  perror("shm_open");
  if (ftruncate(fd, PAGE_SIZE) == -1) {
    perror("ftruncate");
    return EXIT_FAILURE;
  }
#endif

  addr1 = (uintptr_t)mmap(NULL, PAGE_SIZE, PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0);
  if (ptr == MAP_FAILED) perror("mmap addr1");
  addr2 = (uintptr_t)mmap(NULL, PAGE_SIZE, PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0);
  if (ptr == MAP_FAILED) perror("mmap addr2");

  printf("addr1 = %p | addr2 = %p\n", (void*)addr1, (void*)addr2);
  munmap((void*)addr1, PAGE_SIZE);
  munmap((void*)addr2, PAGE_SIZE);

  struct sigaction sa;
  sa.sa_sigaction = HandleSignal;
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = SA_RESTART | SA_SIGINFO;
  if (sigaction(SIGSEGV, &sa, NULL) < 0) {
    perror("sigaction(SIGSEGV)");
  }

  printf("addr1 is shared, addr2 is private (CoW)\n");
  ptr = (int*)mmap((void*)addr1, PAGE_SIZE, PROT_READ|PROT_WRITE, MAP_FIXED|MAP_SHARED, fd, 0);

  if (ptr == MAP_FAILED) perror("mmap");

  printf("got ptr=%p\n", ptr);

  *ptr = 1234;

  printf(" ptr == %i\n", *ptr);

  // throws a fault, then gets a private mapping of memory
  printf("read from addr2 = %i\n", *((int*)addr2));
  *ptr = 12345;
  printf(" ptr == %i\n", *ptr);

  printf("read from addr2 = %i\n", *((int*)addr2));

  // CoW
  *((int*)addr2) = 4321;
  printf("wrote in addr2 = %i\n", *((int*)addr2));
  *ptr = 123456;
  printf(" ptr == %i\n", *ptr);

  printf("read from addr2 = %i (CoWed)\n", *((int*)addr2));

  printf("addr1 = %i (MAP_SHARED) addr2 = %i (MAP_PRIVATE)\n", *((int*)addr1), *((int*)addr2));

  shm_unlink("/myregion");
  return EXIT_SUCCESS;
}
