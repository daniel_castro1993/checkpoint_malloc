#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h>
#include <malloc.h>

int main(int argc, char **argv)
{
  size_t size = 12;
  void *mem = malloc(size);
  printf("Successfully malloc'd %zu bytes at addr %p\n", size, mem);
  assert(mem != NULL);
  free(mem);
  printf("Successfully free'd %zu bytes from addr %p\n", size, mem);

  size = 1024;
  mem = malloc(size);
  printf("Successfully malloc'd %zu bytes at addr %p\n", size, mem);
  assert(mem != NULL);
  free(mem);
  printf("Successfully free'd %zu bytes from addr %p\n", size, mem);

  // for (size = 1048576; size != 0 ; size-=64) {
  // for (size = 0; size < 1048576 ; size+=64) {
  for (size = 4096; size != 0; size-=64) {
  // for (size = 0; size < 4096; size+=64) {
    int *mem[512];
    for (int j = 0; j < 2; ++j) {
      for (int i = 0; i < 512 ; ++i) {
        mem[i] = malloc(size);
        mem[i][size/sizeof(int)] = 512 * j + i;
        assert(mem[i] != NULL);
      }
      for (int i = 0; i < 512 ; ++i) {
        assert(mem[i][size/sizeof(int)] == 512 * j + i);
      }
      for (int i = 0; i < 512 ; ++i) {
        free(mem[i]);
      }
    }
    printf(".");
    fflush(stdout);
  }
  printf("Successful\n");

  malloc_stats();

  return 0;
}
