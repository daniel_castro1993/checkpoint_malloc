# Checkpoint malloc (Notes)

Currently a malloc call works as follows:  
  - 1) check if there is any block available (go over a list, then try to best fit)  
  - 2) if there isn't any, then allocate pages with mmap (one or more)  
  - 3) the block of memory has 16B metadata before the address (size and list pointer)  

The malloc has some metadata per thread, the head of the list of blocks for each block size (8B, 64B, 512B or larger) the arbitrary sized blocks are best fitted on previously freed blocks (if any).  

A free adds the block to the head of the list of the block size.  

For the checkpointer to work, the addresses obtained within the application must be available whith the checkpointer.  
-> I think it is possible if the checkpointer does not allocate any memory (all required memory must be also mapped in the application to avoid overlapping any address)  

## Memory objects

Logs, pages and the root configuration are files stored in /dev/shm.  
logs have the format: log_%p_%zu  
pages have the format: page_%p_%zu  
config file has the format: pmalloc_%p_%zu  

each file in /dev/shm represents a memory segment, open it with shm_open, which returns a file discriptor, then use it in mmap to map the segment into memory. Pages are mapped private in the application and shared in the checkpointer. Logs and config are shared in both.

The behaviour of MAP_PRIVATE allows to write-through if the application is the only one using the memory segment, i.e., it may damage the persistent memory file, hence, be sure the page is MAP_SHARED in the checkpointer before accessing it, i.e., wake up the checkpointer, make sure it processes the malloc, then go back on processing.

## Log structure

Logs are shared between application and checkpointer. There are 4 different entries:  
  i) write value  
  ii) write payload  
  iii) malloc  
  iv) end of transaction  

The malloc must come necessarily before the write (or the checkpointer will crash). However, if this is log parallel, a thread may allocate, send the pointer to other thread, which writes. The write of the second thread cannot be applied before the malloc of the first thread.  

In order to guarantee the previous issue does not happen, the malloc could use a global lock, request the checkpointer to malloc and wait an ack. Only after the checkpointer acks the application thread may proceed.  

The alternative would be: the checkpointer tracks SIGSEGV, if there is some issue then go over some malloc list and mmap (can we do the mmaps on the signal handler?), after the mmap, the offending address can be accessed without problems. What if there is a bug on the checkpointer? It will never exit due to SIGSEGV.  

The checkpointer malloc already uses a global pthread mutex to protect the mmap, so I guess it is safe to add all mallocs in the same log (special log only with the malloc entries, the per thread logs only have writes).  




