# Logged Malloc

This malloc implementation logs all accesses into a shared log with a checkpointer process, which applies the writes in the same address space.

It is meant to be used in the context of Persistent Memory and serves as middleware to Hardware Transactional Memory implementations that do not allow log externalization during the execution of transactions. This way a transaction can use volatile memory pages, which will be eventually be written to persistent memory pages through the checkpointer.

The volatile implementation was forked from https://github.com/savanpatel/malloc (commit df50ced71d75dec767a9fde6b95fbec5d30260df). It was changed to allocate memory through mmap (sbrk was dropped), mmap pages are stored currently in /dev/shm (which is volatile, however, changing this path to point the PM device should suffice).

